var Prism = require('prismjs')
var fs = require('fs')
var path = require('path')
require('prismjs/components/prism-clike')
require('prismjs/components/prism-c')
require('prismjs/components/prism-cpp')
require('prismjs/components/prism-python')
require('prismjs/components/prism-java')


String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function GetLanguage(filename) {
    if (filename.endsWith('.cpp'))
        return Prism.languages.cpp;
    else if (filename.endsWith('.py'))
        return Prism.languages.python;
    else if (filename.endsWith('.java'))
        return Prism.languages.java;
}

function GetLanguageId(filename) {
    if (filename.endsWith('.cpp'))
        return "language-cpp";
    else if (filename.endsWith('.py'))
        return "language-python";
    else if (filename.endsWith('.java'))
        return "language-java";
    return "";
}

var lang = GetLanguage(process.argv[2]);
var lang_id = GetLanguageId(process.argv[2]);
if (lang_id === "") {
    process.exit();
}
var code = fs.readFileSync(process.argv[2]).toString();

var highlighted = Prism.highlight(code, lang);

highlighted = '<pre class="' + lang_id + '"><code class="line-numbers">' + highlighted + '</code></pre>';

var pattern = fs.readFileSync(process.argv[3], "utf8");
var html = pattern.replaceAll("{{title}}", path.basename(process.argv[2])).replaceAll("{{code}}", highlighted);

console.log(html);

#!/usr/bin/python3

from pathlib import Path
import os
from conf import src_dir, dest_dir, css_dir


def rec_install_path(p_start, p_dest):
    if p_start.name == ".git":
        return
    if not p_dest.exists():
        p_dest.mkdir()
    for p_next in p_start.iterdir():
        if p_next.is_dir():
            rec_install_path(p_next, p_dest / p_next.name)
        else:
            hldir = os.path.dirname(os.path.realpath(__file__))
            p_out = p_dest / (p_next.name + ".html")
            os.system("cd {} && node {}/highlight.js {} {}/pattern.html > {}".format(p_out.parent, hldir, p_next.absolute(), hldir, p_out))


def rec_remove_path(p):
    if not p.exists():
        return
    for q in p.iterdir():
        if q.is_dir():
            rec_remove_path(q)
            q.rmdir()
        else:
            q.unlink()


p_start = Path(src_dir)
p_dest = Path(dest_dir)

if p_dest.exists():
    rec_remove_path(p_dest)
    p_dest.rmdir()
rec_install_path(p_start, p_dest)

os.system("cp cosh.css {}".format(css_dir))
